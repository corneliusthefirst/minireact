/**
 *
 * @param {string} type - it is a string that specifies the type of the
 * DOM node we want to create, it`s the tagName you
 * pass to document.createElement when
 * you want to create an HTML element
 * @param {object} props - it is another object, it has all the
 * keys and values from the JSX attributes
 * @param  {...Array||object} children - can be a string or an array with more elements
 * @returns
 */
function createElement(type, props, ...children) {
  return {
    type,
    props: {
      ...props,
      children: children.map((child) =>
        typeof child === "object" ? child : createTextElement(child)
      ),
    },
  };
}

//create texte element
function createTextElement(text) {
  return {
    type: "TEXT_ELEMENT",
    props: {
      nodeValue: text,
      children: [],
    },
  };
}

/**
 *
 * @param { array} fiber - fiber is a tree like structure with root node and
 * childrens we go through as we do for trees
 * @returns returns the update dom
 */
function createDom(fiber) {
  const dom =
    fiber.type === "TEXT_ELEMENT"
      ? document.createTextNode("")
      : document.createElement(fiber.type);

  updateDom(dom, {}, fiber.props);
  return dom;
}

const isEvent = (key) => key.startsWith("on");
const isProperty = (key) => key !== "children" && !isEvent(key);
const isNew = (prev, next) => (key) => prev[key] !== next[key];
const isGone = (prev, next) => (key) => !(key in next);

/**
 *
 * @param {*object} dom
 * @param {*object} prevProps
 * @param {*object} nextProps
 */
function updateDom(dom, prevProps, nextProps) {
  //Remove old or changed event listeners
  Object.keys(prevProps)
    .filter(isEvent)
    .filter((key) => !(key in nextProps) || isNew(prevProps, nextProps)(key))
    .forEach((name) => {
      const eventType = name.toLowerCase().substring(2);
      dom.removeEventListener(eventType, prevProps[name]);
    });

  // Remove old properties
  Object.keys(prevProps)
    .filter(isProperty)
    .filter(isGone(prevProps, nextProps))
    .forEach((name) => {
      dom[name] = "";
    });

  // Set new or changed properties
  Object.keys(nextProps)
    .filter(isProperty)
    .filter(isNew(prevProps, nextProps))
    .forEach((name) => {
      dom[name] = nextProps[name];
    });

  // Add event listeners
  Object.keys(nextProps)
    .filter(isEvent)
    .filter(isNew(prevProps, nextProps))
    .forEach((name) => {
      const eventType = name.toLowerCase().substring(2);
      dom.addEventListener(eventType, nextProps[name]);
    });
}

function commitRoot() {
  deletions.forEach(commitWork);
  commitWork(wipRoot.child);
  currentRoot = wipRoot;
  wipRoot = null;
}

/**
 *
 * @param {object} fiber
 * @returns recursive call to the child and siblings
 */
function commitWork(fiber) {
  if (!fiber) {
    return;
  }

  /*To find the parent of a DOM node we go up the fiber 
    tree until we find a fiber with a DOM node. */
  let domParentFiber = fiber.parent;
  while (!domParentFiber.dom) {
    domParentFiber = domParentFiber.parent;
  }
  const domParent = domParentFiber.dom;

  if (fiber.effectTag === "PLACEMENT" && fiber.dom != null) {
    domParent.appendChild(fiber.dom);
  } else if (fiber.effectTag === "UPDATE" && fiber.dom != null) {
    updateDom(fiber.dom, fiber.alternate.props, fiber.props);
  } else if (fiber.effectTag === "DELETION") {
    /** When removing a node we also need to keep going until
     *  we find a child with a DOM node */
    commitDeletion(fiber, domParent);
  }

  commitWork(fiber.child);
  commitWork(fiber.sibling);
}

function commitDeletion(fiber, domParent) {
  if (fiber.dom) {
    domParent.removeChild(fiber.dom);
  } else {
    commitDeletion(fiber.child, domParent);
  }
}

/**
 *
 * @param {* ReactElement} element - reactElement
 * @param {* ReactNode} node - ReactNode
 */
function render(element, node) {
  /* Work in progress root */

  const state = componentState.get(element) || { cache: [] };
  globalParent = element;
  globalId = 0;

  wipRoot = {
    dom: node,
    props: {
      children: [element],
    },
    alternate: currentRoot,
  };
  deletions = [];
  nextUnitOfWork = wipRoot;
}

/** we break the work into small units,
 * after we finish each unit we’ll let the browser
 * interrupt the rendering if there’s anything else
 * that needs to be done. */
let nextUnitOfWork = null;

let currentRoot = null;
let wipRoot = null;
let deletions = null;

/**
 *
 * @param {number} deadline - supplied by the requestIdleCallback
 * We can use it to check how much time we have until the browser
 * needs to take control again
 */
function workLoop(deadline) {
  let shouldYield = false;
  while (nextUnitOfWork && !shouldYield) {
    /**we set the first unit of work, and then write a
     * performUnitOfWork call that returns the next unit of work */
    nextUnitOfWork = performUnitOfWork(nextUnitOfWork);
    shouldYield = deadline.timeRemaining() < 1;
  }

  if (!nextUnitOfWork && wipRoot) {
    commitRoot();
  }
  /** it like a setTimeout run by the browser when the main thread is idle */
  requestIdleCallback(workLoop);
}

requestIdleCallback(workLoop);

function performUnitOfWork(fiber) {
  const isFunctionComponent = fiber.type instanceof Function;
  if (isFunctionComponent) {
    updateFunctionComponent(fiber);
  } else {
    updateHostComponent(fiber);
  }
  if (fiber.child) {
    return fiber.child;
  }
  let nextFiber = fiber;
  while (nextFiber) {
    if (nextFiber.sibling) {
      return nextFiber.sibling;
    }
    nextFiber = nextFiber.parent;
  }
}

/* Work in progress fiber */
let wipFiber = null;
let hookIndex = null;

/**
 *
 * @param {object} fiber
 * we go different way to update for functional components because
 * -the fiber from a function component doesn’t have a DOM node
 * -and the children come from running the function instead
 *  of getting them directly from the props
 */
function updateFunctionComponent(fiber) {
  /**set the work in progress fiber */
  wipFiber = fiber;
  /**Keep track of the current hook index */
  hookIndex = 0;
  /* We add a hooks array to the fiber to support calling
   * a hook several times in the same component.*/
  wipFiber.hooks = [];
  /*we run the function to get the children */
  const children = [fiber.type(fiber.props)];
  reconcileChildren(fiber, children);
}

/**
 * @param {object} fiber
 * Creates a new node and appens it to the dom
 */
function updateHostComponent(fiber) {
  if (!fiber.dom) {
    fiber.dom = createDom(fiber);
  }
  /**get the childrens directly from the props */
  reconcileChildren(fiber, fiber.props.children);
}

/**
 *
 * @param {object} wipFiber
 * @param {object} elements
 */
function reconcileChildren(wipFiber, elements) {
  let index = 0;
  let oldFiber = wipFiber.alternate && wipFiber.alternate.child;
  let prevSibling = null;

  while (index < elements.length || oldFiber != null) {
    const element = elements[index];
    /**Then for each child we create a new fiber. */
    let newFiber = null;

    const sameType = oldFiber && element && element.type === oldFiber.type;

    /* we add it to the fiber tree setting either as
     * a child or as a sibling, depending on whether it’s
     * the first child or not. */
    if (sameType) {
      newFiber = {
        type: oldFiber.type,
        props: element.props,
        dom: oldFiber.dom,
        parent: wipFiber,
        alternate: oldFiber,
        effectTag: "UPDATE",
      };
    }
    if (element && !sameType) {
      newFiber = {
        type: element.type,
        props: element.props,
        dom: null,
        parent: wipFiber,
        alternate: null,
        effectTag: "PLACEMENT",
      };
    }
    if (oldFiber && !sameType) {
      oldFiber.effectTag = "DELETION";
      deletions.push(oldFiber);
    }

    if (oldFiber) {
      oldFiber = oldFiber.sibling;
    }

    if (index === 0) {
      wipFiber.child = newFiber;
    } else if (element) {
      prevSibling.sibling = newFiber;
    }

    prevSibling = newFiber;
    index++;
  }
}

//useEffect function
const componentState = new Map();
let globalId = 0;
let globalParent;
function useEffect(callback, dependencies) {
  //constante
  const id = globalId;
  const parent = globalParent;
  globalId++;
  //callback
  (() => {
    //stock old state
    const { cache } = componentState.get(parent) || { cache: [] };
    console.log(" useRef " + cache);

    //secure
    if (cache[id] == null) {
      cache[id] = { dependencies: undefined };
    }

    //get last states
    const dependenciesChanged =
      dependencies == null ||
      dependencies.some((dependency, i) => {
        return (
          cache[id].dependencies == null ||
          cache[id].dependencies[i] !== dependency
        );
      });

    //secure and clean
    if (dependenciesChanged) {
      if (cache[id].cleanup != null) cache[id].cleanup();
      cache[id].cleanup = callback();
      cache[id].dependencies = dependencies;
    }
  })();
}

//useState function
function useState(initial) {
  //initale state - check if old hook present
  const oldHook =
    wipFiber.alternate &&
    wipFiber.alternate.hooks &&
    wipFiber.alternate.hooks[hookIndex];

  /*hook with args - If we have an old hook, we copy
    the state from the old hook to the new hook, if 
    we don’t we initialize the state*/
  const hook = {
    state: oldHook ? oldHook.state : initial,
    queue: [],
  };

  /*events - we get all the actions from the old hook queue,
   and then apply them one by one to the new hook state, 
   so when we return the state it’s updated*/
  const actions = oldHook ? oldHook.queue : [];
  actions.forEach((action) => {
    hook.state = action(hook.state);
  });

  //event managed - so we define a setState function that receives an action
  const setState = (action) => {
    hook.queue.push(action);
    wipRoot = {
      dom: currentRoot.dom,
      props: currentRoot.props,
      alternate: currentRoot,
    };
    nextUnitOfWork = wipRoot;
    deletions = [];
  };

  /*last event - Then we add the new hook to the fiber, 
    increment the hook index by one, and return the state*/
  wipFiber.hooks.push(hook);
  hookIndex++;
  //array to use state mutiple time
  return [hook.state, setState];
}

// useRef function
function useRef(initialValue) {
  const hook = {
    current: initialValue,
  };

  wipFiber.hooks.push(hook);
  hookIndex++;

  return hook;
}

// custom hook useForceRender
function useForceRender() {
  const setC = useState(0)[1];
  return () => setC((c) => c + 1);
}

//export functions
export {
  createElement,
  createTextElement,
  createDom,
  render,
  useState,
  useEffect,
  useRef,
  useForceRender,
};
