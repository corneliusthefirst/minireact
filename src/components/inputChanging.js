import * as MiniReact from '../modules/miniReact';

/** @jsx MiniReact.createElement */
export function InputChanging({props}) {

  const inputRef = MiniReact.useRef();

  MiniReact.useEffect(() => {
    inputRef.current && inputRef.current.focus();
  });


  return (
    <div>
        <input className="border mt-3" type="text" ref={inputRef} name={props.name} id={props.id} placeholder={props.placeholder}/>
    </div>

  );

}