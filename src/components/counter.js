import * as MiniReact from '../modules/miniReact';

let count = 0

/** @jsx MiniReact.createElement */
export function Counter() {
    const forceRender = MiniReact.useForceRender()
    return (
      <div className="flex flex-col justify-center items-center m-2">
        <div>{count}</div>
        <button
          className="bg-purple-700 px-6 text-white rounded-md hover:bg-purple-500"
          onClick={() => {
            count++
            forceRender()
          }}
        >
          +1
        </button>
      </div>
    )
}