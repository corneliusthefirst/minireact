import * as MiniReact from "../modules/miniReact";

/** @jsx MiniReact.createElement */
export function HomePage() {
  const [file, setFile] = MiniReact.useState("");
  function onChange(event) {
    var files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      var imageType = /^image\//;

      if (!imageType.test(file.type)) {
        continue;
      }

      var img = document.createElement("img");
      img.classList.add("obj");
      img.file = file;
      document.getElementById("preview").appendChild(img);
      var reader = new FileReader();
      reader.onload = (function (aImg) {
        return function (e) {
          aImg.src = e.target.result;
        };
      })(img);
      reader.readAsDataURL(file);
    }
  }

  return (
    <div className="flex h-screen flex-col items-center justify-center ">
      <p className="text-3xl text-red-400 font-bold underline">
        here the homePage
      </p>
      <button className="my-8" onClick={() => (window.location.href = "/list")}>
        go to list page
      </button>
      <button onClick={() => (window.location.href = "/countdown-page")}>
        go to countdown page
      </button>
      <div className="flex flex-col  w-full items-center justify-center  my-8  pl-32">
        <input type="file" onchange={onChange}></input>
      </div>
      <div className="flex  flex-col my-8 bg-red-400 max-w-96">
        <p>{file}</p>
      </div>
      <div id="preview"></div>
    </div>
  );
}
