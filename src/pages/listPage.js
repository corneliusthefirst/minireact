import * as MiniReact from "../modules/miniReact";
import { InputChanging } from "../components/inputChanging";
import { Counter } from "../components/counter";

/** @jsx MiniReact.createElement */
export default function ListPage() {
  return (
    <div className="flex h-screen flex-col items-center justify-center ">
      <div>
        <p className="text-3xl text-blue-400 font-bold underline mb-5">
          Here the list page
        </p>
      </div>
      <div>
        <p className="text-xl font-bold underline text-center">Counter</p>
        <Counter />
      </div>
      <div>
        <p className="text-xl font-bold underline">Input change</p>
        <InputChanging
          props={{
            name: "test",
            id: "inputChanging",
            placeholder: "Ecrivez votre texte ici",
          }}
        />
      </div>
      <button className="mt-8" onClick={() => (window.location.href = "/")}>
        {"<-"} goBack
      </button>
    </div>
  );
}
