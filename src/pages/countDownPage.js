import * as MiniReact from "../modules/miniReact";

/** @jsx MiniReact.createElement */
export default function CountDownPage() {
  const [time, setTime] = MiniReact.useState(30);
  const [canCount, setCanCount] = MiniReact.useState(false);

  MiniReact.useEffect(() => {
    const myInterval = setInterval(() => {
      if (canCount) {
        if (time > 0) {
          setTime((_time) => _time - 1);
        }
        if (time === 0) {
          //clearInterval(myInterval);
          setCanCount((c) => false);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });

  const startCounter = () => {
    setCanCount((c) => true);
  };

  const resetCounter = () => {
    setTime((c) => 30);
  };

  return (
    <div className="flex h-screen flex-col items-center justify-center ">
      <p className="text-3xl text-black font-bold underline text-4xl">{time}</p>
      <button className="mt-8" onClick={startCounter}>
        Start count
      </button>
      <button className="mt-8" onClick={resetCounter}>
        Reset count
      </button>
      <button className="mt-8" onClick={() => (window.location.href = "/")}>
        {"<-"} goBack
      </button>
    </div>
  );
}
