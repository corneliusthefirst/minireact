import * as MiniReact from "../modules/miniReact";
import ListPage from "../pages/listPage";
import CountDownPage from "../pages/countDownPage";
import Router from "./router";
import { HomePage } from "../pages/homePage";

/** @jsx MiniReact.createElement */
function renderPage(element) {
  const container = document.getElementById("root");
  MiniReact.render(element, container);
}

const router = new Router();
router.get("/", function (req) {
  return renderPage(<HomePage />);
});
router.get("/list", function (req) {
  return renderPage(<ListPage />);
});
router.get("/countdown-page", function (req) {
  return renderPage(<CountDownPage />);
});
export default router;
